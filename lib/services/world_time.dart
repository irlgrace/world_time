import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime{
  String time;
  String flag;
  String location;
  String url;
  bool isDayTime;

  WorldTime({this.location, this.url, this.flag});

  Future<void> getTime() async {
    try{
      Response response = await get('http://worldtimeapi.org/api/timezone/$url');
      Map data = jsonDecode(response.body);
      String datetime = data['datetime'];
      String offset = data['utc_offset'].substring(1, 3);
      String timezone = data['timezone'];

      DateTime now = DateTime.parse(datetime);
      now = now.add(Duration(hours: int.parse(offset)));
      time = DateFormat.jm().format(now);
      isDayTime = now.hour > 5 && now.hour < 18 ? true : false;
    }catch(e){
      print('Caught Error: $e');
      time = 'Cannot get Time';
    }
  }

}