import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map data = {};
  String backgroundImg = '';
  @override
  Widget build(BuildContext context) {
    data = data.isNotEmpty ? data: ModalRoute.of(context).settings.arguments;
    // set background
    backgroundImg = data['isDayTime']? 'day.jpg' : 'night.jpg';
    Color bgColor = data['isDayTime']? Colors.yellow : Colors.indigo;
    Color txtColor =  data['isDayTime']? Colors.black : Colors.white;
    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
        child: Container(
         decoration: BoxDecoration(
           image: DecorationImage(
               image: AssetImage('assets/img/backgroundImg/$backgroundImg'),
               fit: BoxFit.cover
           )
         ),
          child: Padding(
            padding: EdgeInsets.fromLTRB(0.0, 150.0, 0.0, 0.0),
            child: Column(
              children: <Widget>[
                FlatButton.icon(
                    onPressed: () async {
                        dynamic newData = await Navigator.pushNamed(context, '/location');
                        setState(() {
                          data = newData;
                        });
                    },
                    icon: Icon(
                        Icons.edit_location,
                        color: txtColor,
                    ),
                    label: Text(
                        'Edit Location',
                        style: TextStyle(
                          color: txtColor
                        )
                    )
                ),
                SizedBox(height: 20.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                        data['location'],
                        style: TextStyle(
                          fontSize: 30.0,
                          letterSpacing: 3.0,
                          color: txtColor
                        )
                    )
                  ],
                ),
                SizedBox(height: 30.0),
                Text(
                  data['time'],
                  style: TextStyle(
                    fontSize: 50.0,
                    color: txtColor
                  ),
                )
              ],
            ),
          ),
        )
      )
    );
  }
}
