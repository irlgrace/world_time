import 'package:flutter/material.dart';
import 'package:world_time/services/world_time.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> with TickerProviderStateMixin{
  void setupWorldTime() async{
    WorldTime instance = WorldTime(location:'Manila', flag:'philippine.jpg', url:'Asia/Manila');
    await instance.getTime();
    Navigator.pushReplacementNamed(context, '/home', arguments:{
        'location' : instance.location,
        'time' : instance.time,
        'flag' : instance.flag,
        'isDayTime' : instance.isDayTime
    });
  }

  @override
  void initState() {
    super.initState();
    setupWorldTime();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: Center(
        child: SpinKitFadingCircle(
          color: Colors.white,
          size: 50.0,
          controller: AnimationController(vsync: this, duration: const Duration(seconds: 5)),
        ),
      )

    );
  }
}
