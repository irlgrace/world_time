import 'package:flutter/material.dart';
import 'package:world_time/services/world_time.dart';

class ChooseLocation extends StatefulWidget {
  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation> {

  List<WorldTime> locations = [
    WorldTime(url: 'America/Buenos_Aires', location:'Buenos Aires', flag:'argentina.png'),
    WorldTime(url: 'Australia/Canberra', location:'Canberra', flag:'australia.jpg'),
    WorldTime(url: 'Africa/Cairo', location:'Cairo', flag:'egypt.png'),
    WorldTime(url: 'Europe/Athens', location:'Athens', flag:'greece.png'),
    WorldTime(url: 'Asia/Tokyo', location:'Tokyo', flag:'japan.png'),
    WorldTime(url: 'Europe/London', location:'London', flag:'london.png'),
    WorldTime(url: 'Asia/Manila', location:'Manila', flag:'philippine.png'),
    WorldTime(url: 'Europe/Madrid', location:'Madrid', flag:'spain.png'),
    WorldTime(url: 'America/New_York', location:'New York', flag:'usa.png')
  ];

  void updateTime(index) async {
    WorldTime  newLocation = locations[index];
    await newLocation.getTime();
    Navigator.pop(context, {
      'location' : newLocation.location,
      'time' : newLocation.time,
      'flag' : newLocation.flag,
      'isDayTime' : newLocation.isDayTime
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Choose Location'),
        centerTitle: true,
        backgroundColor: Colors.indigo,
      ),
      body: ListView.builder(
          itemCount: locations.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
              child: Card(
                child: ListTile(
                  onTap: () {
                    updateTime(index);
                  },
                  title: Text(locations[index].location),
                  leading: CircleAvatar(
                      backgroundImage: AssetImage('assets/img/flagImg/${locations[index].flag}'),
                  ),
                )
              ),
            );
          }
      )
    );
  }
}
